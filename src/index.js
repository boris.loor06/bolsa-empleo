const express = require('express');
const cors = require('cors')
const morgan = require('morgan')


const empleoRoutes = require('../src/routes/empleos.routes')
//iniciar el servidor
const app = express();

//dependencias
app.use(cors())
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//middleware
app.use((err, req, res, next) => {
    return res.status(500).json({
      status: "error",
      message: err.message,
    });

});
//uso de rutas
app.use(empleoRoutes)
//puerto servidor de datos



app.listen(process.env.PORT || 8000)
