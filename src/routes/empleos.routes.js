const {Router} = require('express');

//importamos los controladores para las rutas
const func = require('../controllers/empleos.controllers')
const { ingresoUsuario, registroEmpresa, registroEstudiante, auth, logout} = require('../controllers/autenticacion.controllers');
const { verifyToken } = require('../controllers/verificarJWT');

const router = Router();

//rutas GET 
router.get('/', verifyToken, func.home)
router.get("/empresa", verifyToken, func.homeEmpresa)
router.get("/perfil", verifyToken, func.getPerfilEst)
router.get("/perfil-empresa", verifyToken, func.getPerfilEmp)
router.get("/empleos", verifyToken, func.getEmpleos)
router.get("/postulaciones", verifyToken, func.getPostulaciones)
router.get("/ofertas", verifyToken, func.getOfertas)
router.get("/oferta/:id", verifyToken, func.getOferta)
router.get("/formacion", verifyToken, func.getFormacionEst)
router.get("/cursos", verifyToken, func.getCursosEst)
router.get("/experiencia", verifyToken, func.getExperienciaEst)

//rutas POST
router.post("/oferta", verifyToken, func.ofertaEmp)
router.post("/edit", verifyToken, func.editEst)
router.post("/editPerfil", verifyToken, func.editEmp)
router.post("/postular", verifyToken, func.postular)
router.post("/postulaciones", verifyToken, func.aceptar)
router.post("/formacion", verifyToken, func.formacion)
router.post("/cursos", verifyToken, func.curso)
router.post("/experiencia", verifyToken, func.experiencia)

//rutas DELETE
router.delete("/empresa", verifyToken, func.deleteOferta)

//rutas de sesión
router.post('/login', ingresoUsuario)
router.post('/registroEstudiante', registroEstudiante)
router.post('/registroEmpresa', registroEmpresa)
router.get('/logout', logout)


module.exports = router



