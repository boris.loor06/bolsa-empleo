const pool = require('../db')
pool.connect();

//retorna las ofertas de empleo para el estudiante
const home = async (req, res, next) => {
    try {
          await pool.query("SELECT * FROM ofertas",(err, result) => {
            if(err) res.json(err)
            res.json(result.rows)})
      } catch (error) {
        next(error);
      }
}

//retorna las oferta de empleo creadas por una empresa especifica
const homeEmpresa = async (req, res, next) => {
    try {
        await pool.query("SELECT * FROM ofertas WHERE id_empresa = $1 ", [req.userId], (err, result) => {if(err) res.json(err)
          res.json(result.rows)});
      } catch (error) {
        next(error);
      }
    }

//retorna los datos de perfil del estudiante
const getPerfilEst = async (req, res, next) => {
    try {
        await pool.query(`select * from usuarios where id_usuario = $1`, [req.userId], (err, result)=>{
          if(err) res.json(err)
          res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}
const getFormacionEst = async (req, res, next) => {
    try {
        await pool.query(`select * from formacion form inner join posee on posee.id_formacion = form.id_formacion where posee.id_usuario = $1`, [req.userId], 
        (err, result) => {
          if(err) res.json(err)
          res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}
const getCursosEst = async (req, res, next) => {
    try {
        await pool.query(`select * from cursos form inner join realizo on realizo.id_cursos = form.id_cursos where realizo.id_usuario = $1`, [req.userId], 
        (err, result) =>{if(err) res.json(err)
           res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}
const getExperienciaEst = async (req, res, next) => {
    try {
        await pool.query(`select * from experiencia form inner join obtuvo on obtuvo.id_exp_laboral = form.id_exp_laboral where obtuvo.id_usuario = $1`, [req.userId], 
        (err, result) =>{if(err) res.json(err)
           res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}

//retorna los datos de perfil de la empresa
const getPerfilEmp = async (req, res, next) => {
    try {
        await pool.query('select * from empresas where id_empresa = $1', [req.userId], (err, result) => {
          if(err) res.json(err)
          else res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}

//retorna las postulaciones que han hecho los estudiantes a una empresa
const getPostulaciones = async (req, res, next) => {
    try {
        await pool.query(`SELECT ofertas.id_oferta, titulo_oferta, nom_usuario as nombre, ape_usuario as apellido, email_usuario as email, telefono_usuario as telefono
        FROM ofertas
        inner join postula on postula.id_oferta = ofertas.id_oferta
        inner join usuarios on usuarios.id_usuario = postula.id_usuario
        WHERE ofertas.id_empresa = $1 and aceptado = FALSE`,
        [req.userId], (err, result) => {if(err) res.json(err)
          else res.json(result.rows)})

      } catch (error) {
        next(error);
      }
}

const getOfertas = async (req, res, next) => {
    try {
        await pool.query("SELECT * FROM ofertas WHERE ofertas.id_empresa = $1 ", 
        [req.userId], (err, result) => {if(err) res.json(err)
         else res.json(result.rows)});
      } catch (error) {
        next(error);
      }
}
const getOferta = async (req, res, next) => {
  const id = req.params.id
  try {
    await pool.query("SELECT * FROM ofertas WHERE ofertas.id_oferta = $1", 
    [id], (err, result) => {if(err) res.json(err)
      else res.json(result.rows)});
  } catch (error) {
    next(error);
  } 
}

//retorna empleos recomendados
const getEmpleos = async(req, res, next) => {
  try {
    await pool.query("SELECT * FROM ofertas ORDER BY id_oferta DESC LIMIT 10", 
    [], (err, result) => {if(err) res.json(err)
      else res.json(result.rows)});
  } catch (error) {
    next(error);
  }
}

//maneja la actualizacion del perfil del estudiante
const editEst = async(req, res, next) => {
  try {
    const {nombre, apellido, correo, telefono} = req.body

    await pool.query(
        "UPDATE usuarios SET nom_usuario = $1, ape_usuario=$2, email_usuario=$3, telefono_usuario=$4 WHERE id_usuario=$5 RETURNING *",
        [nombre, apellido, correo, telefono, req.userId],
        (err, result) => {
          if(err) res.json(err)
          else res.json(result.rows)}
    );

} catch (error) {
    next(error);
}
}

//maneja la actualizacion del perfil de la empresa
const editEmp = async(req, res, next) => {
  try {
    const {
        nombre,
        apellido,
        cedula,
        correo,
        ruc,
        empresa,
        sede,
        telefono,
        pais,
        provincia,
        ciudad,
        direccion,
        historia
    } = req.body
    
    await pool.query(
        "UPDATE empresas SET nombre_emp = $1, apellido_emp=$2, ced_emp=$3, email_emp=$4, ruc_emp=$5, nom_emp=$6, sede_emp=$7, tel_emp=$8, pais_emp=$9, prov_emp=$10, ciudad_emp=$11, dir_emp=$12, historia_emp=$13 WHERE id_empresa=$14 RETURNING *",
        [nombre, apellido, cedula, correo, ruc, empresa, sede, telefono, pais, provincia, ciudad, direccion, historia, req.userId],
        (err, result) => {
          if(err) res.json(err)
          else res.json(result.rows)}
    );

} catch (error) {
    next(error);
}
}
const aceptar = async(req, res, next) => {
  const {bool, id} = req.body
  try{
    await pool.query("UPDATE postula SET aceptado = $1 where id_oferta = $2 returning *", [bool, id],
    async (err, result) => {
      if(err) res.json(err);
      if(result.rowCount>0){
        await pool.query("UPDATE ofertas set vac_oferta = vac_oferta - 1 where id_oferta = $1 returning *", [id])
      }
      res.json(result.rows)})
  }catch (error){
    next(error)
  }
}
const postular = async(req, res, next) => {
  const {id} = req.body
  try {
    await pool.query("INSERT INTO POSTULA VALUES ($1, $2, $3) RETURNING *",
    [id, req.userId, 'false'], (err, result) => 
    {if(err) res.json(err)
    else res.json(result.rows);})
  } catch (error) {
    next(error);
  }
}

//creacion de una oferta de empleo de una empresa
const ofertaEmp = async (req, res, next) => {
    try {
        const {
            titulo,
            puesto,
            vacantes,
            provincia,
            localidad,
            sueldo,
            funciones,
            requisitos,
        } = req.body
           await pool.query(
            "INSERT INTO ofertas(id_empresa, titulo_oferta, puesto_oferta, vac_oferta, prov_oferta, loc_ofertas, sueldo_ofertas, func_ofertas, req_ofertas) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *",
            [req.userId, titulo, puesto, vacantes, provincia, localidad, sueldo, funciones, requisitos], (err, result) => {
              if(err) res.json(err)
              else res.json(result.rows)});
    } catch (error) {
        next(error);
    }
}

const deleteOferta = async(req, res, next) => {
  try {
    const {id} = req.body
    console.log(id)
    await pool.query('begin')
    const enc = await pool.query('select * from postula where id_oferta = $1', [id])
    if(enc.rowCount>0){
      const data = await pool.query("delete from postula where id_oferta = $1", [id])
      if(data.rowCount>0){
        await pool.query("delete from ofertas where id_empresa = $1 and id_oferta = $2",
          [req.userId, id],
          async (err, result) => {if(err){
              await pool.query('rollback')
              res.json(err)}
              else{
                await pool.query('commit')
                res.json(result.rows)
              } 
            });
          }
    }else{
      await pool.query("delete from ofertas where id_empresa = $1 and id_oferta = $2",
        [req.userId, id],
        async (err, result) => {if(err){
          await pool.query('rollback')
          res.json(err)}
          else{
            await pool.query('commit')
            res.json(result.rows)
          }
        }
      );
    }
    } catch (error) {
        await pool.query('rollback')
      next(error);
  }
}

const formacion = async(req, res, next) => {
  try {
    const {estudio} = req.body
    await pool.query('begin')
        const result = await pool.query("insert into formacion(estudios) values ($1) returning *",
          [estudio]);
          let data
          if(result.rowCount===1){
            data = await pool.query('insert into posee values ($1, $2) returning *',
              [result.rows[0].id_formacion, req.userId]);
          }
        await pool.query('commit')
        res.json(data)
  } catch (error) {
      await pool.query('rollback')
      next(error);
  }
}
const curso = async(req, res, next) => {
  try {
    const {curso} = req.body
    await pool.query('begin')
        await pool.query("insert into cursos(cursos) values ($1) returning *",
          [curso],(err, result) => {if(err) res.json(err)});
          await pool.query('insert into realizo values ($1, $2)',
          [result.id_cursos, req.userId], (err, result) => {if(err) res.json(err)})
            await pool.query('commit')
            res.json({"ok": "ok"})
  } catch (error) {
      await pool.query('rollback')
      next(error);
  }
}
const experiencia = async(req, res, next) => {
  try {
    const {empresa, tiempo, func} = req.body
    await pool.query('begin')
        await pool.query("insert into experiencia(nom_empresa, tiempo_laboral, funciones) values ($1, $2 ,$3) returning*",
          [empresa, tiempo, func], (err, result) => {if(err) res.json(err)});
        await pool.query('insert into obtuvo values ($1, $2)',
        [result.id_exp_laboral, req.userId], (err, result) => {if(err) res.json(err)})
      await pool.query('commit')
      res.json({"ok": "ok"})
  } catch (error) {
      await pool.query('rollback')
      next(error);
  }
}


module.exports = {
    home,
    homeEmpresa,
    getPerfilEmp,
    getPerfilEst,
    getEmpleos,
    getPostulaciones,
    getCursosEst,
    getExperienciaEst,
    getFormacionEst,
    getOfertas,
    ofertaEmp,
    editEst,
    editEmp,
    deleteOferta,
    getOferta,
    postular,
    formacion,
    curso, 
    experiencia,
    aceptar
}