import React from 'react';

class SearchEstudiantes extends React.Component{

    constructor(props){
        super(props);

        this.onChangeEvent = this.onChangeEvent.bind(this);
    }

        onChangeEvent(e){
            this.props.onsearch(e.target.value);
        }

    render(){
        return(
            <input type="text" onChange={this.onChangeEvent} />
        );
    }
}

export default SearchEstudiantes;