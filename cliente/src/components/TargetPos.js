import React from 'react'

function TargetPos({datos, showInfo}) {
    const info = () => showInfo(datos.id_oferta)
    return (
        <div>
            <div className="post">
                <div className="ctn-img">
                    <img src={require("../img/Captura.PNG")} alt="postulacion"/>
                </div>
                <h2>{datos.nombre}</h2>
                <h3>Postulado para {datos.titulo_oferta}</h3>
                <button id="btn_info" onClick={info}>Más Informacion</button>
            </div>
        </div>
    )
}

export default TargetPos
