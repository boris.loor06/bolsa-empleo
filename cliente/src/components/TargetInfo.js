import React from 'react'
import { Boton } from '../elementos/Formularios'

function TargetInfo({datos, handleAcept, handleDeny}) {
    const acept = () => handleAcept(datos.id_oferta)
    const deny = () => handleDeny(datos.id_oferta)
    return (
            <div className="post-contenido">
                <div className="ctn-img">
                    <img src={require("../img/logo.png")} alt="postulacion"/>
                </div>
                <h1>Postulante para {datos.titulo_oferta}</h1>
                <div className="data_info">
                    <ul>
                        <h2>Datos personales</h2>
                        <p><strong>Nombres:</strong> {datos.nombre}</p>
                        <p><strong>Apellidos:</strong> {datos.apellido}</p>
                        <p><strong>Teléfono:</strong> {datos.telefono}</p>
                        <p><strong>Email:</strong> {datos.email}</p>
                    </ul>
                </div>
                <Boton className="formulario__btn" onClick={acept}>Aceptar</Boton>
                <Boton className='cancel' onClick={deny}>Rechazar</Boton>
            </div>
    )
}

export default TargetInfo
