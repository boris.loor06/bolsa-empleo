import React from 'react';
import Item from './Item';
import '../css/List.css';

function List(props){
    
    return(
        <div className="list">
            {props.items.map(item =>
                <Item 
                    key={item.id_oferta} 
                    id={item.id_oferta} 
                    title={item.titulo_oferta} 
                    puesto={item.puesto_oferta}
                    sueldo={item.sueldo_ofertas}
                    provincia={item.prov_oferta}
                    localidad={item.loc_ofertas}
                    onremove={props.onremove}
                    detalle={props.detalle}
                    isEmp ={props.isEmp} 
                    />
            )}
        </div>
    );
}


export default List;