import React from 'react';
import '../css/Item.css';
import '../css/home.css'

class Item extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            title: '',
            provincia: '',
            sueldo: '',
            puesto: '',
            localidad: ''
        }

        this.onremove = this.onremove.bind(this);
        this.detalle = this.detalle.bind(this);
        this.onChangeRating = this.onChangeRating.bind(this);
    }

    componentDidMount(){
        this.setState({
            id: this.props.id,
            title: this.props.title,
            provincia: this.props.provincia,
            sueldo: this.props.sueldo,
            puesto: this.props.puesto,
            localidad: this.props.localidad,
        });
    }

    onremove(e){
        if(!window.confirm('Seguro desea eliminar?')){
            return
        }else{
            this.props.onremove(this.props.id);
        }
    }
    detalle(e){
        this.props.detalle(this.props.id);
    }
    onChangeRating(e){
        const rating = parseInt(e.target.value)
        this.setState({
            rating: parseInt(e.target.value),
            stars: Array(parseInt(e.target.value)).fill(1)
        });

        this.props.onupdaterating({id: this.state.id, title: this.state.title, image: this.state.image, rating: rating});
    }

    render(){
        return(
            <div className="item">
                <div className="ctn-img"><img src={require("../img/logo1.PNG")} alt="logo"/></div>
                <div className="title">
                    <h3>
                        {this.state.title}
                    </h3>
                </div>
                <div className="puesto"><p>{this.state.puesto}</p></div>
                <div className="detalle"><p>Provincia: {this.state.provincia}</p></div>
                {this.props.isEmp? <div/>:<div className="detalle"><p>Localidad: {this.state.localidad}</p></div>}
                <div className="detalle"><p>Sueldo: {this.state.sueldo}</p></div>
                {this.props.isEmp?
                <div className="actions"><button className='cancel' onClick={this.onremove}>Eliminar</button></div>:
                <div className="actions"><button className='formulario__btn' onClick={this.detalle}>Info</button></div>}
            </div>
        );
    }

}

export default Item;