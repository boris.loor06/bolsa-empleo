import React, {useEffect, useState} from 'react'
import { useNavigate } from 'react-router-dom'

import Header from '../components/header'

function EditEstudiante() {
    const navigate = useNavigate();
    const token = localStorage.getItem('user')
    const userType = localStorage.getItem('userType')
    const auth = localStorage.getItem('auth')
    const [user, setUser] = useState({})
    const [exp, setExp] = useState([])
    const [curs, setCurso] = useState([])
    const [estu, setEstudio] = useState([])

    const loadFormacion = async () => {
      const forma = await fetch("https://bolsa-empleo-api.herokuapp.com/formacion", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        }});
     const data = await forma.json()
        setEstudio(data)
    }
    const loadCurso = async () => {
      const curs = await fetch("https://bolsa-empleo-api.herokuapp.com/cursos", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },});
      const cursos = await curs.json();
      setCurso(cursos)
    }

    const loadExp = async() =>{ 
      const exp = await fetch("https://bolsa-empleo-api.herokuapp.com/experiencia", {headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },});
      const expe = await exp.json();
      setExp(expe)
    }
    
    const loadPerfil = async () => {
      const response = await fetch("https://bolsa-empleo-api.herokuapp.com/perfil", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },});
      const data = await response.json();

      setUser({nombre: data[0].nom_usuario,
              apellido: data[0].ape_usuario,
              correo: data[0].email_usuario,
              telefono: data[0].telefono_usuario,
      })
  };

  const handleChange = (e) =>
  setUser({...user, [e.target.name]: e.target.value });


  const handleSubmit = async(e) => {
    e.preventDefault();
    const response = await fetch("https://bolsa-empleo-api.herokuapp.com/edit", {
        method: "POST",
        headers: { "Content-Type": "application/json",
        'x-access-token': token },
        body: JSON.stringify(user),
      });
      const data = await response.json();
    if(data) navigate('/perfil-estudiante')
  }

const cancel = (e) =>{
  navigate("/perfil-estudiante")
}
const añadirCur = (e) =>{
  navigate("/edit-estudiante/curso")
}
const añadirEst = (e) =>{
  navigate("/edit-estudiante/estudio")
}
const añadirExp = (e) =>{
  navigate("/edit-estudiante/experiencia")
}

    useEffect((() => {
        if(auth){
          if(userType==='estudiante'){
            loadPerfil();
            loadCurso();
            loadExp();
            loadFormacion();
          }else{
            navigate('/home-empresa')
          }
        }else{
          navigate('/')
        }

      }),[auth]);

    return (
        <div>
            <Header active="perfil"/>
            <main>
        <h1>Actualizar perfil</h1>
        <div className="img-perfil">
            <img src={require("../img/perfil.png")} alt="foto de perfil" width="100" height="100"/>
        </div>
        <div className="datos-perfil">
            <h2>Datos Personales</h2>
            <form onSubmit={handleSubmit}>
            <div className="personales">
                <h3>Nombres</h3>
                <input type="text" name="nombre" onChange={handleChange} value={user.nombre}></input>
                <h3>Apellidos</h3>
                <input type="text" name="apellido" onChange={handleChange} value={user.apellido}></input>
                <h3>Teléfono</h3>
                <input type="text" name="telefono" onChange={handleChange} value={user.telefono}></input>
                <h3>Correo Eléctronico</h3>
                <input type="text" name="correo" onChange={handleChange} value={user.correo}></input>
            </div>
            <h2>Formación Academica</h2>
            <div className="estud">
                <h3>Estudios</h3>
                {estu.map((e, index)=>{
                  return(
                    <input key={index} type="text" name="estudios" onChange={handleChange} value={e.estudios}></input>
                  )
                })}
                <button id='add' onClick={añadirEst}>Agregar</button>
            </div>
            <h2>Cursos y capacitaciones</h2>
            <div className="curs">
            {curs.map((e, index)=>{
              return(
                <input key={index} type="text" name='curso' onChange={handleChange} value={e.cursos}></input>
              )
            })}
                <button id='add' onClick={añadirCur}>Agregar</button>
            </div>
            <h2>Experiencia Laboral</h2>
            {exp.map((e, index)=>{
              return(
            <div className="exp" key={index}>
                <h3>Empresa</h3>
                <input type="text" onChange={handleChange} value={e.nom_empresa}></input>
                <h3>Tiempo Laboral</h3>
                <input type="text" onChange={handleChange} value={e.tiempo_laboral}></input>
                <h3>Funciones realizadas</h3>
                <textarea type="text" onChange={handleChange} value={e.funciones}></textarea>
                <button id='add' onClick={añadirExp}>Agregar</button>
            </div>
              )
            })}
            <button id='save' type='submit'>Guardar</button>
            <button id="cancel" onClick={cancel}>Cancelar</button>
            </form>
        </div>
    </main>
        </div>
    )
}

export default EditEstudiante
