import React, {useEffect, useState} from 'react'
import { useNavigate } from 'react-router-dom'

import Header from '../components/header'
import TargetInfo from '../components/TargetInfo';
import TargetPos from '../components/TargetPos';
import '../css/postulaciones.css'

function Postulaciones() {
    const navigate = useNavigate();
    const token = localStorage.getItem('user')
    const userType = localStorage.getItem('userType')
    const auth = localStorage.getItem('auth')
    const [est, setEst] = useState([])
    const [inf, setInfo] = useState(false)
    const [res, setRes] = useState({})
    const [actual, setActual] = useState({})

    const loadEst = async() =>{
      const response = await fetch('https://bolsa-empleo-api.herokuapp.com/postulaciones', {
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        }
      })
      const data = await response.json();
      setEst(data);
    }
    const info = (id) => {
      setInfo(!inf)
      setActual({id})
    }
    const enviar = async() =>{
      const response = await fetch('https://bolsa-empleo-api.herokuapp.com/postulaciones', {
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
        method: 'POST',
        body: JSON.stringify(res)
      })
      const data = await response.json();
      if(data) navigate('/postulaciones')
    }
    const acept = (id) => {
      setRes({bool: true, id: id})
      enviar()
    }
    const deny = (id) => {
      setRes({bool: null, id: id})
      enviar()
    }

    useEffect(() => {
        if(auth){
          if(userType==='empresa'){
            loadEst()
          }else{
            navigate('/home-estudiante')
          }
        }else{
          navigate('/')
        }
      },[]);
    return (
        <div>
            <Header active={'empleos'} isEmp={true}/>
            <div className="posts">
            {est.length===0&&
            <div><h3>No hay postulaciones aun</h3></div>}
            {est.map(e=>{
              return(
              <TargetPos datos={e} showInfo={info}/>
              )
            })}
            {inf && est.map(e=>{
              if(e.id_oferta===actual.id){
              return(
                <TargetInfo datos={e} handleAcept={acept} handleDeny={deny}/>
              )
              }else{
                return <div></div>
              }
            })}
            {console.log(res)}
            </div>
        </div>
    )
}

export default Postulaciones
