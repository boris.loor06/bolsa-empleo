import React, {useEffect, useState} from 'react'
import { useNavigate } from 'react-router-dom'

import Header from '../components/header'
import '../css/editarEmpresa.css'

function EditEmpresa() {
    const navigate = useNavigate();
    const token = localStorage.getItem('user')
    const userType = localStorage.getItem('userType')
    const auth = localStorage.getItem('auth')
    const [user, setUser] = useState({})

    const loadPerfil = async () => {
      const response = await fetch("https://bolsa-empleo-api.herokuapp.com/perfil-empresa", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },});
      const data = await response.json();
      setUser({nombre: data[0].nombre_emp,
        apellido: data[0].apellido_emp,
        cedula: data[0].ced_emp,
        correo: data[0].email_emp,
        telefono: data[0].tel_emp,
        ruc: data[0].ruc_emp,
        empresa: data[0].nom_emp,
        sede: data[0].sede_emp,
        pais: data[0].pais_emp,
        provincia:data[0].prov_emp,
        ciudad: data[0].ciudad_emp,
        direccion: data[0].dir_emp,
        historia: data[0].historia_emp
      })
  };
  
  const handleSubmit = async(e) => {
    e.preventDefault();
    const response = await fetch("https://bolsa-empleo-api.herokuapp.com/editPerfil", {
        method: "POST",
        headers: { "Content-Type": "application/json",
        'x-access-token': token },
        body: JSON.stringify(user),
      });
      const data = await response.json();
    if(data) navigate('/perfil-empresa')
  }

  const cancel = (e) =>{
		navigate("/perfil-empresa")
	}

  const handleChange = (e) =>
  setUser({...user, [e.target.name]: e.target.value });

    useEffect(() => {
        if(auth){
          if(userType==='empresa'){
            loadPerfil();
          }else{
            navigate('/home-estudiante')
          }
        }else{
          navigate('/')
        }
      },[]);

    return (
        <div>
            <Header active={'perfil'} isEmp={true}/>
            <main>
        <h1>Actualizar perfil</h1>
        <div className="img-perfil">
            <img src={require("../img/perfil.png")} alt="foto de perfil" width="100" height="100"/>
        </div>
        <div className="datos-perfil">
        <h2>Datos de la empresa</h2>
        <form onSubmit={handleSubmit}>
            <div className="personales">
              <h3>Empresa</h3>
              <input onChange={handleChange} name="empresa" value={user.empresa} type="text"></input>
              <h3>RUC</h3>
              <input onChange={handleChange} name="ruc" type="text" value={user.ruc}></input>
              <h3>Sede</h3>
              <input onChange={handleChange} name="sede" type="text" value={user.sede}></input>
              <h3>Telefono</h3>
              <input onChange={handleChange} name="telefono" type="text" value={user.telefono}></input>
              <h3>Pais</h3>
              <input onChange={handleChange} name="pais" type="text" value={user.pais}></input>
              <h3>Provincia</h3>
              <input onChange={handleChange} name="provincia" type="text" value={user.provincia}></input>
              <h3>Ciudad</h3>
              <input onChange={handleChange} name="ciudad" type="text" value={user.ciudad}></input>
              <h3>Dirección</h3>
              <input onChange={handleChange} name="direccion" type="text" value={user.direccion}></input>
            </div>
        <h2>Gerente Empresa</h2>
            <div className="personales">
              <h3>Nombres</h3>
              <input onChange={handleChange} name="nombre" type="text" value={user.nombre}></input>
              <h3>Apellidos</h3>
              <input onChange={handleChange} name="apellido" type="text" value={user.apellido}></input>
              <h3>Cédula</h3>
              <input onChange={handleChange} name="cedula" type="text" value={user.cedula}></input>
              <h3>Correo</h3>
              <input onChange={handleChange} name="correo" type="text" value={user.correo}></input>
            </div>
            <h2>Descripcion de la Empresa</h2>
            <div className="descripcion">
                <h3>Breve historia y Objetivo</h3>
                <textarea onChange={handleChange} name="historia" rows="5" type="text" value={user.historia}></textarea>
            </div>
            <button type="submit" id="save">Guardar</button>
        </form>
            <button id="cancel" onClick={cancel}>Cancelar</button>
            </div>
            </main>
        </div>
    )
}

export default EditEmpresa
