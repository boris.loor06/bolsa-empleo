import React, {useEffect, useState} from 'react'
import { useNavigate } from 'react-router-dom'

import Header from '../components/header'
import List from '../components/List';


function Empleos() {
    const navigate = useNavigate();
    const token = localStorage.getItem('user')
    const userType = localStorage.getItem('userType')
    const auth = localStorage.getItem('auth')
    const [empleos, setEmpleos] = useState([])

    const loadEmpleos = async () => {
      const response = await fetch('https://bolsa-empleo-api.herokuapp.com/empleos', {
        headers: {"Content-Type": "application/json",
                  'x-access-token': token}
      });
      const data = await response.json()
      setEmpleos(data);
    }
    const detalle = (id) => {
      localStorage.setItem("oferta", id)
      navigate(`/detalle`)
  }
    useEffect(() => {
        if(auth){
          if(userType==='estudiante'){
            loadEmpleos()
          }else{
            navigate('/home-empresa')
          }
        }else{
          navigate('/')
        }

      },[]);
    return (
        <div>
            <Header active="empleos"/>
            <div className='container-info-cover'>
            <h1>Empleos para ti</h1>
            </div>
            <List className="list" items={empleos} detalle={detalle} isEmp={false}/> 
        </div>
    )
}

export default Empleos
