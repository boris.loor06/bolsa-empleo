import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Header from '../components/header'
import { Boton } from '../elementos/Formularios';

function Detalle() {
    const [oferta, setOferta] = useState([]);
    const token = localStorage.getItem('user')
    const id = localStorage.getItem('oferta')
    const navigate = useNavigate()

    const loadOfert = async() => {
        const response = await fetch(`https://bolsa-empleo-api.herokuapp.com/oferta/${id}`, {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token,
        },
        });
        const data = await response.json();
        setOferta(data);
    }
    const postular = async() => {
        const response = await fetch("https://bolsa-empleo-api.herokuapp.com/postular", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body :JSON.stringify({id})
        });
        const data = await response.json();
        if(data){navigate('/home-estudiante')}
    }

    const cancel = () => {
        navigate('/home-estudiante')
    }
    useEffect(()=>{
        loadOfert();
    },[])
    return (
        <div>
        <Header active="empleos"/>
        <main>
            <div>
                <img className="img-oferta" src={require("../img/fondo.png")} alt="foto oferta" width="100" height="100"></img>
            </div>
            <h1>Postulacion</h1>
            <div className="datos-perfil">
                {oferta.map(e =>{
                  return(
                  <div className="personales">
                      <h3>Oferta</h3>
                      <p>{e.titulo_oferta}</p>
                      <h3>Puesto</h3>
                      <p>{e.puesto_oferta}</p>
                      <h3>Localidad</h3>
                      <p>{e.loc_ofertas}</p>
                      <h3>Provincia</h3>
                      <p>{e.prov_oferta}</p>
                      <h3>Requisitos</h3>
                      <p>{e.req_ofertas}</p>
                      <h3>Funciones</h3>
                      <p>{e.func_ofertas}</p>
                      <h3>Sueldo</h3>
                      <p>{e.sueldo_ofertas}</p>
                      <h3>Vacantes</h3>
                      <p>{e.vac_oferta}</p>
                  </div>
                  )
                })}
            </div>
                <Boton className="formulario__btn" onClick={postular}>Postular</Boton>
                <Boton className="cancel"  onClick={cancel}>Cancelar</Boton>
        </main>
        </div>
    )
}

export default Detalle
