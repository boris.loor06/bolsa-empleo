import React, {useState, useEffect} from 'react'
import { useNavigate } from 'react-router-dom'
import List from '../components/List';
import Header from '../components/header'

import "../css/home.css"

function Home({isEmp}) {
  document.body.classList.remove('fondo');
  document.body.classList.remove('body');
    const navigate = useNavigate();
    const token = localStorage.getItem('user')
    const userType = localStorage.getItem('userType')
    const auth = localStorage.getItem('auth')
    const [empleos, setEmpleos] = useState([]);

    const loadEmpleos = async () => {
      const response = await fetch("https://bolsa-empleo-api.herokuapp.com/empresa", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },});
      const data = await response.json();
      setEmpleos(data);
    };
    const loadEst = async () => {
      const response = await fetch("https://bolsa-empleo-api.herokuapp.com/", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },});
      const data = await response.json();
      setEmpleos(data);
    };
    const detalle = (id) => {
      localStorage.setItem("oferta", id)
      navigate(`/detalle`)
  }
    const add = () => {
      navigate('/oferta')
    }
    const remove = async(id) => {
      const response = await fetch("https://bolsa-empleo-api.herokuapp.com/empresa", {headers: {
          'Content-Type': 'application/json',
          'x-access-token': token
        },
        method: 'delete',
        body: JSON.stringify({id})});
      const data = await response.json();
      if (data){
        navigate('/')
      }
    }

    useEffect(() => {
        if(auth){
          if(userType==='empresa'){
            loadEmpleos();
          }else if(userType==='estudiante'){
            loadEst();
          }
        }else{
          navigate('/')
        }
      },[]);
    return (
      <div>
      {isEmp?
      <div>
        <Header active={'home'} isEmp={true}/>
        <div className='container-info-cover'>
          <button onClick={add} className="formulario__btn">+Añadir</button>
        </div>
        {/* <Menu title="WLF-JOBS" onadd={add}/> */}
        <List className="list" items={empleos} onremove={remove} isEmp={true}/> 
      </div>
        :
      <div>
        <h2>Inicio de estudiantes</h2>
        <Header active="home"/>
        <List className="list" items={empleos} isEmp={false} detalle={detalle}/> 
      </div>
      }
      </div>
    )
    
}

export default Home;