import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Header from '../components/header';

function AddPropPerfil({curso, exp, est}) {
    const token = localStorage.getItem('user')
    const navigate = useNavigate();
    const [curs, setCurso] = useState({curso: ''});
    const [estudio, setEstudio] = useState({estudio: ''});
    const [exper, setExp] = useState({empresa: '', tiempo: '', func: ''});

    const handleSubmitCur = async(e) => {
        e.preventDefault();

    const response = await fetch("https://bolsa-empleo-api.herokuapp.com/curso", {
        method: "POST",
        headers: { "Content-Type": "application/json",
        'x-access-token': token },
        body: JSON.stringify(curs),
      });
      const data = await response.json();
      if(data) navigate('/edit-estudiante')
    }
    const handleSubmitExp = async(e) => {
        e.preventDefault();

        const response = await fetch("https://bolsa-empleo-api.herokuapp.com/experiencia", {
            method: "POST",
            headers: { "Content-Type": "application/json",
            'x-access-token': token },
            body: JSON.stringify(exper),
          });
          const data = await response.json();
        if(data) navigate('/edit-estudiante')
    }
    const handleSubmitEst = async(e) => {
        e.preventDefault();

        const response = await fetch("https://bolsa-empleo-api.herokuapp.com/formacion", {
            method: "POST",
            headers: { "Content-Type": "application/json",
            'x-access-token': token },
            body: JSON.stringify(estudio),
          });
          const data = await response.json();
        if(data) navigate('/edit-estudiante')
    }
    const handleChangeCurso = (e) => setCurso({...curs, [e.target.name]: e.target.value });
    const handleChangeExp = (e) => setExp({...exper, [e.target.name]: e.target.value });
    const handleChangeEst = (e) => setEstudio({...estudio, [e.target.name]: e.target.value });
    const cancel = (e) => navigate('/edit-estudiante')
    return (
        <div>
        <Header active={'perfil'} isEmp={true}/>
        <main>
        <div className="datos-perfil">
            {curso ? <form onSubmit={handleSubmitCur}> 
            <h3>Nuevo Curso</h3>
            <input onChange={handleChangeCurso} name="curso"  type="text" placeholder="Ingrese curso realizado"></input>
            <button id='save' type='submit'>Guardar</button>
            <button id="cancel" onClick={cancel}>Cancelar</button>
            </form>
            
            : exp ? <form onSubmit={handleSubmitExp}>
            <h3>Experiencia Laboral</h3>
            <input onChange={handleChangeExp} name="empresa"  type="text" placeholder="Nombre de la empresa"></input>
            <input onChange={handleChangeExp} name="tiempo"  type="text" placeholder="Duración empleo"></input>
            <textarea onChange={handleChangeExp} name="func"  type="text" placeholder="Funciones realizadas"></textarea>
            <button id='save' type='submit'>Guardar</button>
            <button id="cancel" onClick={cancel}>Cancelar</button>
            </form>
            
            : <form onSubmit={handleSubmitEst}>
            <h3>Estudio Realizado</h3>
            <input onChange={handleChangeEst} name="estudio"  type="text" placeholder="Ingrese estudio culminado"></input>
            <button id='save' type='submit'>Guardar</button>
            <button id="cancel" onClick={cancel}>Cancelar</button>
            </form>
        }
            </div>
        </main>
        </div>
    )
}

export default AddPropPerfil
